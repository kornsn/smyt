from django.db.models import Count

from products.models import Product


def question_a():
    """Filter products with price >= 100, group by category and count
    size of each category.
    """
    return (
        Product.objects
            .filter(price__gte=100)
            .values('category__name')
            .annotate(count=Count('category'))
    )


def question_b():
    """Filter products with price >= 100, group by category, count size of each
    category and filter category with size > 10.
    """
    return (
        Product.objects
            .filter(price__gte=100)
            .values('category__name')
            .annotate(count=Count('category'))
            .filter(count__gt=10)
    )
