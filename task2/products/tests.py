from django.test import TestCase

from products import answers_for_questions
from products.models import Category, Product


# Create your tests here.
class TestAnswersForQuestions(TestCase):
    def setUp(self):
        category1 = Category.objects.create(name='category-1')
        category2 = Category.objects.create(name='category-2')

        Product.objects.create(category=category1, name='p1', price=1)
        Product.objects.create(category=category1, name='p2', price=2)
        Product.objects.create(category=category1, name='p3', price=100)
        Product.objects.create(category=category1, name='p4', price=100)
        Product.objects.create(category=category1, name='p5', price=100)
        Product.objects.create(category=category1, name='p6', price=100)
        Product.objects.create(category=category1, name='p7', price=100)
        Product.objects.create(category=category1, name='p8', price=100)
        Product.objects.create(category=category1, name='p9', price=100)
        Product.objects.create(category=category1, name='p10', price=100)
        Product.objects.create(category=category1, name='p11', price=100)
        Product.objects.create(category=category1, name='p12', price=100)
        Product.objects.create(category=category1, name='p13', price=100)

        Product.objects.create(category=category2, name='p21', price=1)
        Product.objects.create(category=category2, name='p22', price=2)
        Product.objects.create(category=category2, name='p23', price=100)
        Product.objects.create(category=category2, name='p24', price=100)
        Product.objects.create(category=category2, name='p25', price=100)

    def test_question_a(self):
        actual = list(
            answers_for_questions.question_a().order_by('category__name')
        )
        expected = [
            {'category__name': 'category-1', 'count': 11},
            {'category__name': 'category-2', 'count': 3},
        ]
        self.assertEqual(actual, expected)

    def test_question_b(self):
        actual = list(
            answers_for_questions.question_b().order_by('category__name')
        )
        expected = [
            {'category__name': 'category-1', 'count': 11},
        ]
        self.assertEqual(actual, expected)
