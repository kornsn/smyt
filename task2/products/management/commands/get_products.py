from django.core.management import BaseCommand
from tabulate import tabulate

from products.models import Product


class Command(BaseCommand):
    help = 'Get list of all products'

    def handle(self, *args, **options):
        products = Product.objects.values(
            'category__name', 'name', 'price'
        ).order_by('category__name', 'name')
        formatted_table = tabulate(
            [
                (
                    product['category__name'],
                    product['name'],
                    product['price'],
                )
                for product in products
            ],
            headers=['category', 'name', 'price'])
        self.stdout.write(formatted_table)
