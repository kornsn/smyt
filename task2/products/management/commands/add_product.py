from django.core.management import BaseCommand

from products.models import Category, Product


class Command(BaseCommand):
    help = 'Add product'

    def add_arguments(self, parser):
        parser.add_argument('category')
        parser.add_argument('name')
        parser.add_argument('price', type=float)

    def handle(self, *args, **options):
        category, _ = Category.objects.get_or_create(
            name=options['category'],
        )
        Product.objects.create(
            category=category,
            name=options['name'],
            price=options['price'],
        )
