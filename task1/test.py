import pytest

from task1 import task

fns = [
    task.remove_unclosed,
    task.remove_unclosed_regex
]

expressions = [
    # initial, processed
    (
        # from task
        'esdfd((esdf)(esdf',
        'esdfd((esdf)',
    ),
    (
        # normal
        'Lorem ipsum (dolor (sit amet), (consectetur) adipiscing) elit',
        'Lorem ipsum (dolor (sit amet), (consectetur) adipiscing) elit',
    ),
    (
        # closed less
        'Lorem ipsum (dolor (sit amet), (consectetur adipiscing elit',
        'Lorem ipsum (dolor (sit amet), ',
    ),
    (
        # closed more
        'Lorem ipsum (dolor (sit amet), (consectetur)) adipiscing) elit))',
        'Lorem ipsum (dolor (sit amet), (consectetur)) adipiscing) elit))',
    ),
    (
        # closed before opened
        'Lorem ipsum) (dolor (sit amet), (consectetur) adipiscing) elit',
        'Lorem ipsum) (dolor (sit amet), (consectetur) adipiscing) elit',
    ),
]


@pytest.mark.parametrize('fn', fns)
@pytest.mark.parametrize('init_expr, expected_result', expressions)
def test(fn, init_expr, expected_result):
    assert fn(init_expr) == expected_result
