import re


def remove_unclosed(s):
    rv = ''
    buffer = ''
    state = 'closed'
    for c in s:
        if c == '(':
            state = 'opened'
            buffer += c
        elif c == ')':
            state = 'closed'
            rv += buffer
            rv += c
            buffer = ''
        else:
            if state == 'opened':
                buffer += c
            elif state == 'closed':
                rv += c
    return rv


def remove_unclosed_regex(s):
    pattern = re.compile(r'\(([^()]*?)*$')
    m = pattern.search(s)
    if m:
        print(m)
        return s[:m.start()]
    else:
        return s
